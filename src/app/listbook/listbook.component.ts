import { Component, OnInit, Input } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-listbook',
  templateUrl: './listbook.component.html',
  styleUrls: ['./listbook.component.css']
})
export class ListbookComponent implements OnInit {

@Input()listBook=[];

  constructor(private data:DataService) { }


  ngOnInit() {
    this.listBook=this.data.listBook;
    // console.dir(this.listBook)
  }


}
