import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {
  book={
    id:0,
    kind:'',
    title:'',
    authors:'',
    publisher:'',
    imageLinks:'',
    description:''
  }
  constructor() { }

  ngOnInit() {
  }

}
