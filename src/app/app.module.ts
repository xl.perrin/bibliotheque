import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import { DataService } from './data.service';
import {Routes, RouterModule} from "@angular/router";
import {FormsModule, ReactiveFormsModule} from "@angular/forms"

import { AppComponent } from './app.component';
import { ListbookComponent } from './listbook/listbook.component';
import { BookComponent } from './book/book.component';
import { AccueilComponent } from './accueil/accueil.component';
import { SearchComponent } from './search/search.component';
import { HttpClientModule } from '@angular/common/http';

const routes:Routes=[
  {path:"", component:ListbookComponent},
  {path:"detail",component:BookComponent},
  {path:"search",component:SearchComponent}
]
@NgModule({
  declarations: [
    AppComponent,
    ListbookComponent,
    BookComponent,
    AccueilComponent,
    SearchComponent
  ],
  imports: [
    RouterModule.forRoot(routes),
    BrowserModule,FormsModule, ReactiveFormsModule, HttpClientModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
