import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';

@Injectable()
export class DataService {
  constructor(private http : HttpClient) { }
//add book to list achats
  listBook=[];
  searchValueSubjet = new Subject<String>()

  addBook = (book) => {
    let maxId = (this.getListBook()[0] == undefined) ? 0 : this.getListBook()[0].id;
    // this.getListBook().forEach(element => {
    //   if(element.id > maxId) {
    //     maxId = element.id;
    //   }
    // });
    // annonce.id = maxId + 1;
    let liste = this.getListBook();
    liste.push(book);
    localStorage.setItem("book", JSON.stringify(liste)); 
  }
  getListBookServer = (value) => {     
    return this.http.get('https://www.googleapis.com/books/v1/volumes/?q='+value);
  }
  getListBook=()=>{
    return (JSON.parse(localStorage.getItem("book")) != null) ? JSON.parse(localStorage.getItem("annonces")) : []
  }

}
