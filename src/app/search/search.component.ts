import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { EventEmitter } from 'events';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
book;
list=[];
  constructor(private data : DataService) { }

  ngOnInit() {
  }
  change = (event) => {
    this.book = event.target.value;
  }
  search=()=>{
    this.data.getListBookServer(this.book).subscribe((res:any)=>{
      this.data.listBook = res.items
      console.dir(this.data.listBook  )
    })
  }

}
